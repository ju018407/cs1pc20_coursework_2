#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>

/** \fn int rename_project(char *oldname, char *newname)
    \This function deletes a git repository with the name entered

     This function takes two strings as input, which is the currentname and the newname of the
     project or feature. First it checks that the currentname of the project or feature exists
     or not. If yes then it checks whether a folder with the newname exists or not. If not then it
     changes the name of the folder. At last, it checks if it is a feature or not. if not then it
     changes the name of the branch as well.
*/

int rename_project(char *oldname, char *newname)
{
  char project_check[200];    //An array of type char which will store the path of .git file, to check if it is a git repository
  char rename_branch_command[200];    //An array of type char to store the branch rename command
  int ret;    //Defining an int which will have the rename function.

  FILE*open_project;    //A pointer used to give access to the file descriptor of the pipe

  if(open_project = fopen(("%s", oldname), "r"))  //An if statement to check whether the project name the user inputs to change the name exists or not
  {
    if(open_project = fopen(("%s", newname), "r"))    //An if statement to check whether a project with the same name already exists as the new name the user wants to rename with.
    {
      fclose(open_project);    //Closing the open_project pointer
      printf("A file with the same name already exists, Aborting\n");  //A print statement to let the user know that a different project with the same name already exists.
    }

    else
    {
      snprintf(project_check, 200, "%s/.git", oldname);  //snprintf to store the path of .git file in project_check array

      if(open_project = fopen(("%s", project_check), "r"))  //An if statement to check if it the project the user want to rename is a git repository
      {
        ret = rename(oldname, newname);    //this is to rename the project the user wants to
      }

      else
      {
        ret = rename(oldname, newname);    //this is to rename the project the user wants to
        snprintf(rename_branch_command, sizeof(rename_branch_command), "git branch -m %s %s", oldname, newname);  //snprintf to store the git branch rename command to rename_branch_command array
        system(rename_branch_command);  //this executes the rename_branch_command and renames the branch
      }
      printf("Operation successfull\n");  //Print statement to let the user know that operation successfull
      return 0;    //this returns 0 and ends the function
    }
  }

  else
  {
    printf("The file with the name entered does not exist please try again\n");    //A print statrement to let the user know that the project they want to rename does not exist
  }
}
