/** \my_functions.h is a header file that has all the functions required for project management
     application
*/

int find_tag(char *tag);
int add_feature(char *feature_name);
int add_tag(char *tag);
int create_project(char *project_name);
int delete_project(char *project_name);
int move_by_tag(char *tag_1, char *tag_2);
int rename_by_tag(char *tag, char *new_name);
int rename_project(char *oldname, char *newname);
int move_project(char *oldname, char *newname);
int output_svg(char *file_name);
int add_estimate(char *tag, char *time);
int calculate_all_estimates(char *project_name);
int output_gantt(char *file_name);
int draw_list_files(char *name);
