#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <stdbool.h>
#include <libgen.h>

void goThroughFiles(char *basePath, char *tag_1, char *tag_2);
char* search_the_tag(int num, char *path, char *tag_to_find);

char path_of_tag1[1024];    //Defining a global array that is used to store the path of tag1
char path_of_tag2[1024];    //Defining a global array that is used to store the path of tag2

/** \fn int move_by_tag(char *tag_1, char *tag_2)
    \This function moves the folder of tag1 inside the tag2.

    \This function takes two strings as input which are tag1 and tag2. Then it finds the tag of
     both the tags, then make a new path which will be the path after the file has been moved.
     And finally execute the rename function which moves the tag folder inside other tag the the
     user wants.
*/

int move_by_tag(char *tag_1, char *tag_2)
{
  char current_path[1024];    //An array of type char to store the current path
  char start_path[1024];    //An array of type char which stores the path to start searching for when searching the tag.
  getcwd(current_path, sizeof(current_path));    //getcwd to store the current path in array

  char *token = strtok(current_path, "/");    //strtok is used to get the first part of the path to start searching for tag
  snprintf(start_path, sizeof(start_path), "/%s", token);    //snprintf to add a '/' to create a path to start searching for tag
  goThroughFiles(start_path, tag_1, tag_2);    //Calling the function goThroughFiles, to start searching for tag and find its path
  return 0;    //It returns 0 and ends the function
}

/** \fn void goThroughFiles(char *basePath, char *tag_1, char *tag_2)

    \This function searches through all files and folders for all .pm_tag files. And executes the rename function which moves the folder
*/

void goThroughFiles(char *basePath, char *tag_1, char *tag_2)
{
  char file_path[1000];    //An array of type char to store the path of the .pm_estimate files as it go through all the files
  struct dirent *dp;    //defining a struct to return information about directory entries.
  DIR *dir = opendir(basePath);   //defining a pointer of DIR data type and assigning it to open the basePath which is the parameter of this function

  if (!dir)  //If it is Unable to open directory stream
  {
    return;  //This ends the function.
  }

  while ((dp = readdir(dir)) != NULL)    //A while loop which will run until the directory is NULL
  {
    if (dp->d_type == DT_DIR && strcmp(dp->d_name, ".") !=0 && strcmp(dp->d_name, "..") !=0)//An if statement that executes if the current file is a directory or it does not begin with . or ..
    {
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value

      goThroughFiles(file_path, tag_1, tag_2);  //Call the function recursively
    }

    else if(strcmp(dp->d_name, ".pm_tag") == 0)
    {
      char basename_tag1[1024];    //An array of type char to store the basename of tag1
      char moved_path_tag1[10000];    //An array of type char to store the new path after the tag folder has been moved to other.
      int ret;    //Defining an int which will have the rename function.
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value

      search_the_tag(1, file_path, tag_1);    //Call the search_tag function to find exactly the tag path that we want.
      search_the_tag(2, file_path, tag_2);    //Call the search_tag function to find exactly the tag path that we want.

      snprintf(basename_tag1, sizeof(basename_tag1), "%s", basename(path_of_tag1));  //snprintf to store the basename of tag1 to basename_tag1 array
      snprintf(moved_path_tag1, sizeof(moved_path_tag1), "%s/%s", path_of_tag2, basename_tag1);  //snprintf to store the new path of the tag which will be inside other tag

      if((path_of_tag1 != NULL)&&(path_of_tag2 != NULL))    //An if statement that make sures to execute the rename command only when path of both the tags are found. 
      {
        ret = rename(path_of_tag1, moved_path_tag1);    //this rename function moves the tag1, inside tag2.
      }
    }
  }
  closedir(dir);    //close the dir
}

/** \fn search_the_tag(int num, char *path, char *tag_to_find)

    \This function recives the paths of all the .pm_tag files, but it finds the path of the
     tag that the user is interested in by comparing the found tag in the file with what the user
     inputs
*/


char* search_the_tag(int num, char *path, char *tag_to_find)
{
  FILE*open_tag_file;    //defining a pointer of FILE data type to open the tag files
  char tag_in_file[100];    //An array of type char to store the tag found inside the .pm_tag file

  open_tag_file = fopen(("%s", path), "r");    //opening the file to read the content inside it
  fgets(tag_in_file, 100, open_tag_file);    //fgets takes the content inside the file opened in the pointer above and saves it inside tag_in_file
  tag_in_file[strlen(tag_in_file) - 1] = '\0';    //To remove the NULL character at the end of tag_in_file value because it might cause trouble when handling it
  fclose(open_tag_file);     //Closing the open_tag_file pointer

  if(num == 1)    //Execute this if the num parameter value is 1.
  {
    if(strcmp(tag_in_file, tag_to_find) == 0)      //An if statement which gets execute if the tag found in file is equal to the tag the user inputs
    {
      snprintf(path_of_tag1, sizeof(path_of_tag1), "%s", dirname(path));    //snprintf to store the path of first tag, to path_of_tag1 array
    }
  }

  if(num == 2)
  {
    if(strcmp(tag_in_file, tag_to_find) == 0)    //An if statement which gets execute if the tag found in file is equal to the tag the user inputs
    {
      snprintf(path_of_tag2, sizeof(path_of_tag2), "%s", dirname(path));    //snprintf to store the path of first tag, to path_of_tag2 array
    }
  }
}
