#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>

/** \fn int move_project(char *oldname, char *newname)
    \This function moves the project from one path to another path.

     This function takes two strings as inputs which are the current path and new path. It first
     checks if the current path exists or not. If yes then it checks if the new path has another
     project with the same name. If not then it moves the project.
*/

int move_project(char *oldname, char *newname)
{
  int ret;    //Defining an int which will have the rename function.

  FILE*open_project;    //A pointer used to give access to the file descriptor of the pipe

  if(open_project = fopen(("%s", oldname), "r"))  //An if statement to check whether the project path the user inputs to move exists or not
  {
    if(open_project = fopen(("%s", newname), "r"))    //An if statement to check whether a project with the same name already exists at the new path of the project.
    {
      fclose(open_project);
      printf("A file with the same name already exists in new location, Aborting\n");  //A print statement to let the user know that a different project with the same name already exists in that location.
    }

    else
    {
      ret = rename(oldname, newname);    //this is to rename the project the user wants to
      printf("Operation successfull\n");  //Print statement to let the user know that operation successfull
      return 0;    //this returns 0 and ends the function
    }
  }

  else
  {
    printf("The file with the name entered does not exist please try again\n");    //A print statrement to let the user know that the project they want to move does not exist
  }
}
