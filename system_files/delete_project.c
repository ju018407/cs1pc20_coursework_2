#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <unistd.h>

/** \fn int delete_project(char *project_name)
    \This Function deletes the project or Feature that the user enters.

     This function takes a string which is the project name as the input. Then first it checks
     whether that project exists or not. If yes then it deletes the project and also deletes
     the branch if it is a feature.
*/

int delete_project(char *project_name)
{
  char project_check[200];  //An array of type char to store the path of .git file inside git project

  FILE*open_project;  //A pointer used to give access to the file descriptor of the pipe

  if(open_project = fopen(("%s", project_name), "r"))  //An if statement to check whether the project name entered exists
  {
    fclose(open_project);    //closing the open_project pointer
    char delete_command[200];    //An array of type char to store the delete command for project
    char delete_branch[200];    //An array of type char to store the delete command for branch

    snprintf(project_check,200, "%s/.git", project_name);  //it saves the path of .git file in project_check array
    snprintf(delete_command, 200, "rm -r %s", project_name);  //it saves the delete command to delete project

    FILE*open_git;    //Defining a pointer of FILE data type

    if(open_git = fopen(("%s", project_check), "r"))  //an if statement to check if the project is a git repository
    {
      system(("%s", delete_command));    //execute the delete command
      fclose(open_git);  //closing the open_git pointer
    }

    else
    {
      system(("%s", delete_command));    //execute the delete command
      snprintf(delete_branch, 200, "git branch -d %s", project_name);    //it stores the command to delete the branch
      system(("%s", delete_branch));    //execute the delete branch command
    }
  }

  else
  {
    printf("Error: The name of the project you entered does not exist\n");  //A print statement to tell the user that the project does not exist
  }
  return 0;    //It returns 0 and ends the function
}
