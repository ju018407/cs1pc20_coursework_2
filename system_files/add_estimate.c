#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <stdbool.h>
#include <libgen.h>
#include "my_functions.h"

void checkAllFiles(char *basePath, char *tag, int sub_folder_level);
char* search_The_tag(char *path, char *tag_to_find);

char path_of_tag[1024];    //Defining a global variable of type char to store the path of tag folder and use it in other function which is the add estimate function to add the estimate in the text file in that path.


/** \fn int add_estimate(char *tag, char *time)
    \This Function adds a time/workload estimate to the tag folder the user inputs

    \This function takes two strings as an input which is the tag and the time to add. First it
     searches for that tag folder and saves its path. then it checks whether there is already a
     estimate file in that tag folder. If not then it creates a new estimate file at that path
     and saves the time in it.
*/
int add_estimate(char *tag, char *time)
{
  char estimate_file[2024];    //An array of type char to store the path of the .pm_estimate file

  char current_path[1000];    //An array of type char to store the current_path the user is in
  char start_path[1024];    //An array of type char which stores the path to start searching for when searching the tag.
  getcwd(current_path, sizeof(current_path));    //getcwd command to get the current path

  char *token = strtok(current_path, "/");    //strtok function used to break the path by '/' and to get the first part of the path to start the search
  snprintf(start_path, sizeof(start_path), "/%s", token);    //snprintf to add a / to the path to start searching for

  checkAllFiles(start_path, tag, -2);    //Calling the function checkAllFiles, to start searching for tag and find its path

  snprintf(estimate_file, sizeof(estimate_file), "%s/.pm_estimate", path_of_tag);  //snprintf to store the path of .pm_estimate file inside array estimate_file
  FILE*open_tag_folder;    //A pointer used to give access to the file descriptor of the pipe

  if(open_tag_folder = fopen(("%s", estimate_file), "r"))    //An if statement to check if the .pm_estimate file exists
  {
    char estimate[100];    //An array of type char to store the time stored inside the .pm_estimate file.
    fgets(estimate, 100, open_tag_folder);    //fgets takes the content inside the file opened in the pointer and saves it in estimate array
    printf("There is already a time estimate added to this tag folder: %s", estimate);    //A print statement to let the user know a estimate already exists for that tag
    fclose(open_tag_folder);    //It closes the open_tag_folder pointer
  }

  else    //it executes it, if the estimate file does not exist
  {
    FILE *open_file = fopen(("%s", estimate_file), "w");  //declaring a pointer and opening the estimate_file
    fprintf(open_file, "%s\n", time);    //fprintf stores the time the user inputs in the path assigned to the pointer
    fclose(open_file);    //It closes the open_file pointer
//    calculate_all_estimates();    //Calling a function called calculate_all_estimates which adds all the time estimates of one subfolder level and puts it into above level
  }

  return 0;    //It returns 0 and ends the function
}

/** \fn void checkAllFiles(char *basePath, char *tag, int sub_folder_level)

    \This function searches through all files and folders and finds for all .pm_estimate files
     found
*/

void checkAllFiles(char *basePath, char *tag, int sub_folder_level)
{
  sub_folder_level++;    //increases the subfolder level by 1
  char file_path[1000];  //An array of type char to store the path of the .pm_estimate files as it go through all the files
  struct dirent *dp;    //defining a struct to return information about directory entries.
  DIR *dir = opendir(basePath);    //defining a pointer of DIR data type and assigning it to open the basePath which is the parameter of this function

  if (!dir)  //If it is Unable to open directory stream
  {
    return;  //return and ends the function.
  }

  while ((dp = readdir(dir)) != NULL)    //A while loop which will run until the directory is NULL
  {
    if (dp->d_type == DT_DIR && strcmp(dp->d_name, ".") !=0 && strcmp(dp->d_name, "..") !=0)  //An if statement that executes if the current file is a directory or it does not begin with . or ..
    {
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value

      checkAllFiles(file_path, tag, sub_folder_level);  //Call the function recursively
    }

    else if(strcmp(dp->d_name, ".pm_tag") == 0)    //Execute this if the dp->d_name is a .pm_tag file
    {
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");   //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value

      search_The_tag(file_path, tag);  //Call the search the tag function to find exactly the tag path that we want.
    }
  }
  closedir(dir);
}

/** \fn char* search_The_tag(char *path, char *tag_to_find)

    \This function recives the paths of all the .pm_tag files, but it finds the path of the
     tag that the user is interested in by comparing the found tag in the file with what the user
     inputs
*/

char* search_The_tag(char *path, char *tag_to_find)
{
  FILE*open_tag_file;    //defining a pointer of FILE data type to open the tag files
  char tag_in_file[100];  //An array of type char to store the tag found inside the .pm_tag file

  open_tag_file = fopen(("%s", path), "r");  //opening the file to read the content inside it
  fgets(tag_in_file, 100, open_tag_file);  //fgets takes the content inside the file opened in the pointer above and saves it inside tag_in_file
  tag_in_file[strlen(tag_in_file) - 1] = '\0';  //To remove the NULL character at the end of tag_in_file value because it might cause trouble when handling it
  fclose(open_tag_file);    //closing the open_tag_file pointer

  if(strcmp(tag_in_file, tag_to_find) == 0)  //An if statement which gets execute if the tag found in file is equal to the tag the user inputs
  {
    snprintf(path_of_tag, sizeof(path_of_tag), "%s", dirname(path));    //snprintf to store the path of the tag folder to path_of_tag folder
  }
}
