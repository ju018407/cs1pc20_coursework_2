#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <stdbool.h>
#include <libgen.h>

int tag_check = 0;    //declaring a global variable that checks if that tag is found or not.
void searchThroughFiles(char *basePath, char *tag_name);
char* search_tag(char *filepath, char *tag_find);

/** \fn int find_tag(char *tag)
    \This function finds the path of the tag that the user inputs.

    \This function takes a string as an input which is the tag to find. This function first calls
     other function that goes through all the files and folders, then checks if a tag was found
     or not. If not then it prints that tag not found.
*/
int find_tag(char *tag)
{
  tag_check = 0;  //Assigning 0 to the tag_check variable

  char current_path[1000];    //An array of type char to store the current path
  char start_path[1000];    //An array of type char which stores the path to start searching for when searching the tag.
  getcwd(current_path, sizeof(current_path));    //getcwd to get the path the user is currently in

  char *token = strtok(current_path, "/");    //strtok to get the first part of the path to start looking for tag
  snprintf(start_path, sizeof(start_path), "/%s", token);    //snprintf to store '/' in path to start searching for tag

  searchThroughFiles(start_path, tag);    //Calling the function searchThroughFiles, to start searching for tag and find its path

  if(tag_check == 0)    //if the value of tag_check is 0, then execute this statement
  {
    printf("%s tag does not exist\n", tag);    //A print statement to let the user know tag is not found
    return 1;    //It returns 1, if the tag is not found and ends the function
  }

  return 0;    //It returns 0 and ends the function
}

/** \fn void searchThroughFiles(char *basePath, char *tag_name)

    \This function searches through all files and folders for all .pm_tag files
*/

void searchThroughFiles(char *basePath, char *tag_name)
{
  char file_path[1000];    //An array of type char to store the path of the .pm_estimate files as it go through all the files
  struct dirent *dp;    //defining a struct to return information about directory entries.
  DIR *dir = opendir(basePath);   //defining a pointer of DIR data type and assigning it to open the basePath which is the parameter of this function

  if (!dir)  //If it is Unable to open directory stream
  {
    return;  //This ends the function.
  }

  while ((dp = readdir(dir)) != NULL)    //A while loop which will run until the directory is NULL
  {
    if (dp->d_type == DT_DIR && strcmp(dp->d_name, ".") !=0 && strcmp(dp->d_name, "..") !=0)//An if statement that executes if the current file is a directory or it does not begin with . or ..
    {
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value
      searchThroughFiles(file_path, tag_name);  //Call the function recursively
    }

    else if(strcmp(dp->d_name, ".pm_tag") == 0)    //Execute this if the dp-d_name is a .pm_tag file
    {
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");   //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value
      search_tag(file_path, tag_name);    //Call the search_tag function to find exactly the tag path that we want.
    }
  }

  closedir(dir);  //close the dir
}

/** \fn search_tag(char *filepath, char *tag_find)

    \This function recives the paths of all the .pm_tag files, but it finds the path of the
     tag that the user is interested in by comparing the found tag in the file with what the user
     inputs
*/

char* search_tag(char *filepath, char *tag_find)
{
  FILE*open_tagfile;    //defining a pointer of FILE data type to open the tag files
  char tag_in_file[100];    //An array of type char to store the tag found inside the .pm_tag file

  open_tagfile = fopen(("%s", filepath), "r");    //opening the file to read the content inside it
  fgets(tag_in_file, 100, open_tagfile);    //fgets takes the content inside the file opened in the pointer above and saves it inside tag_in_file
  tag_in_file[strlen(tag_in_file) - 1] = '\0';    //To remove the NULL character at the end of tag_in_file value because it might cause trouble when handling it

  fclose(open_tagfile);    //Closing the open_tagfile pointer

  if(strcmp(tag_in_file, tag_find) == 0)    //An if statement which gets execute if the tag found in file is equal to the tag the user inputs
  {
    tag_check = 1;    //Overwrite the value of tag_check to 1 from 0
    printf("Found tag: %s in the path shown below\n", tag_in_file);    //A print statement to let the user know that the tag has been found
    printf("%s\n", dirname(filepath));    //Print statement to print the path the tag is in
  }
}
