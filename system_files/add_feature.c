#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <string.h>

/** \fn int add_feature(char *feature_name)
    \This Functions creates a Folder and branch with the name entered

     This function takes a string as an input then first checks whether that feature exists
     or not. If not then it creates a feature with the string entered and also it creates six
     different folders inside, and finally creates the branch of the same feature name.
*/

int add_feature(char *feature_name)
{
  FILE*open_feature;      //A pointer used to give access to the file descriptor of the pipe

  if(open_feature = fopen(("%s", feature_name), "r"))    //An if statement to check whether that Feature exists or not
  {
    fclose(open_feature);    //It closes the open_feature pointer
    printf("A feature of that name already exists. Aborting\n");  //Prints the string that tells the user that the Feature exists
  }

  else      //Execute this else statement if the Feature does not exist
  {
    mkdir(("%s", feature_name),0777);      //Creates the Feature with the name that the user entered

    char subfolder_name[][6] = {"bin", "doc", "lib", "src", "test", "config"};    //A 2D array of type char that has all the names stored of the subfolders that will be creates inside the Feature
    for(int i=0;i<=6;i++)      //A for loop to create all the subfolder one by one inside the Feature, with each for loop iteration
    {
      char create_subfolder[200];    //An Array of type char to store the path that the subfolder needs to be created at
      snprintf(create_subfolder, 200, "%s/%s", feature_name, subfolder_name[i]);    //snprintf concatenate the strings to create the path. It has the Feature name, then a slash, then the subfolder name, of the current fo loop iteration and stores it inside create_subfolder array.
      mkdir(("%s", create_subfolder), 0777);    //Creates the subfolder on the path defined above
    }
    char create_branch[200];    //An array of type char to store the branch command with the Feature name
    snprintf(create_branch, 200, "git branch %s", feature_name);     //snprintf concatenate the git branch command and the feature name and stores it in create_branch array
    system(create_branch);    //This executes the command stored in create_branch using the function stored in system library.
  }
}
