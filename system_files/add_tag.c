#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <string.h>

/** \fn int add_tag(char *tag)
    \This Function adds a tag to the directory that the user is currently on

     This function takes as a string which is the tag, as the input. Then first it checks
     whether the current directory already has a tag or not. If not then it creates a new
     file called .pm_tag and writes the tag inside it.
*/

int add_tag(char *tag)
{
  FILE*open_current_directory;   //A pointer used to give access to the file descriptor of the pipe

  if(open_current_directory = fopen(".pm_tag", "r"))  //An if statement to check whether the current directory has .pm_tag file or not
  {
    char tag_check[255];    //An array of type char used as a buffer for reading from the pipe
    FILE * open_file = fopen(".pm_tag", "r");    //This opens the file .pm_tag and reads the content inside it
    fgets(tag_check, 255, open_file);    //fgets takes the content inside the file opened in the pointer above and saves it inside tag_check
    fclose(open_current_directory);    //It closes the open_current_directory pointer
    printf("Already a tag for this folder\n");  //a print statement to let the user know that the tag already exists
    printf("%s", tag_check);  //this prints the tag that is stored in the tag_check array.
    fclose(open_file);    //It closes the open_file pointer
  }

  else
  {
    FILE * open_file = fopen(".pm_tag", "w");    //This creates and opens the file .pm_tag and gives writing access to the file
    fprintf(open_file, "%s\n", tag);       //fprintf writes the tag inside the .pm_tag file that the user enters.
    fclose(open_file);       //It closes the open_file pointer.
  }
  return 0;      //It returns the 0 value and ends the function
}
