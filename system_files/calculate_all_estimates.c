#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <stdbool.h>
#include <libgen.h>
#include <stdlib.h>

char *searchAllFiles(char *basePath, int sub_directory_level);
char *locate_tag(char *filepath);
char *check_all_estimates(char *tag_path, int sub_directory_lvel);
char *check_estimate_of_a_sublevel(char *basePath, int sub_level);

char ignore[100];    //Defining a global array to store the path of .pm_estimate file that does not need to be added when calculating the total
int all_estimates[100];    //Defining a global array to store all the estimates to add them together
int highest_sub_directory_level;    //Defining an int to store the highest sub directory level
int i = 0;    //Defining an int to store the estimates in the correct place in all_estiates array

/** \fn int calculate_all_estimates(void)
    \This Function adds all the time estimates of a sub directory level and stores that in the
     above sub_directory level

    \This function starts to search for the highest sub directory level existing, after that it
     starts to look for all the .pm_estimate files in that highes sub directory and adds them and
     stores them in the above level. Then it repeats again untill the subdirectory_level reaches
     the highest level. In this way all estimates are calculated untill we get the addition of
     all the estimates of a project and stored inside that project.
*/
int calculate_all_estimates(char *project_name)
{
  highest_sub_directory_level = 0;    //assigning 0 to highest_sub_directory_level variable
  char current_path[1000];    //An array of type char to store the current path the user is in
  char start_path[2000];    //An array of type char which stores the path to start searching for when searching the tag.

  getcwd(current_path, sizeof(current_path));    //getcwd to get the current path
  snprintf(start_path, sizeof(start_path), "%s/%s", current_path, project_name);    //snprintf to store the path of the project the user inputs

  searchAllFiles(start_path, 0);    //Calling the function searchAllFiles, to start searching for estimates and find the highest the sub_directory level
  while(highest_sub_directory_level>0)    //A while loop to run until the highest sub directory level is greater than 0
  {
    check_all_estimates(start_path, 0);  //Calling this function to check the estimates of one sublevel and add them and put them in above level
    i=0;    //overwriting the value of i by 0, so that it stores the estimates again from 0 position.
    highest_sub_directory_level--;    //decreasing the value of highest_sub_directory_level by 1
  }

//  system("rm -r .pm_estimate");

  return 0;    //It returns 0 and ends the function
}

/** \fn char *searchAllFiles(char *basePath, int sub_directory_level)

    \This function searches through all files and folders and finds for all .pm_estimate files and notes the highest sub_directory level
*/

char *searchAllFiles(char *basePath, int sub_directory_level)
{
  sub_directory_level++;    //increases the subfolder level by 1
  char file_path[1000];  //An array of type char to store the path of the .pm_estimate files as it go through all the files
  struct dirent *dp;    //defining a struct to return information about directory entries.
  DIR *dir = opendir(basePath);    //defining a pointer of DIR data type and assigning it to open the basePath which is the parameter of this function

  while ((dp = readdir(dir)) != NULL)    //A while loop which will run until the directory is NULL
  {
    if (dp->d_type == DT_DIR && strcmp(dp->d_name, ".") !=0 && strcmp(dp->d_name, "..") !=0)  //An if statement that executes if the current file is a directory or it does not begin with . or ..
    {
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value

      searchAllFiles(file_path, sub_directory_level);  //Call the function recursively
    }

    else if(strcmp(dp->d_name, ".pm_estimate") == 0)
    {
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value

      if(sub_directory_level > 1)    //An if statement to check if the sub_directory level is greater than 1
      {
        char create_estimate_file[2024];    //An array of type char to store the path of .pm_estimate file that needs to created.
        snprintf(create_estimate_file, sizeof(create_estimate_file), "%s/.pm_estimate", dirname(dirname(file_path)));  //Snprintf is used to store the path of the folder which is one sublevel above, with the .pm_estimate file so that it can be created if needed.

        FILE*open_file_path;    //defining a pointer of FILE data type to open the path stored above in create_estimate_file

        if(!(open_file_path = fopen(("%s", create_estimate_file), "r")))    //An if statement to execute if the .pm_estimate file does not exist in path stored in create_estimate_file array 
        {
          FILE *open_file = fopen(("%s", create_estimate_file), "w");    //This creates a estimate_file at the path stored in create_estimate_file array
          fprintf(open_file, "0\n");    //fprintf puts 0 value inside the newly created estimate file
          fclose(open_file);    //It closes the open_file pointer
        }
      }

      if(sub_directory_level > highest_sub_directory_level)    //An if statememnt used to ensure that only the highest sub_directory level is stored
      {
        highest_sub_directory_level = sub_directory_level;    //Overwrite the value in highest_sub_directory_level variable
      }

    }
  }
  sub_directory_level--;    //decrease the vlue of sub_directory_level by 1
  closedir(dir);    //close the dir
}

/** \fn char* locate_tag(char *filepath)
    \This Function finds the estimate inside the .pm_estimate file and saves it in a 2D array

    \This function takes one string as parameter which is the path of various .pm_estimate files. Then it fetches the estimate inside them and changes its data type from string to int, then stores it in a 2D array
*/

char* locate_tag(char *filepath)
{
  FILE*open_estimatefile;    //defining a pointer of FILE data type to open the estimate files
  char estimate_inside[100];    //An array of type char to store the estimate found inside the .pm_estimate file

  open_estimatefile = fopen(("%s", filepath), "r");    //opening the file to read the content inside it
  fgets(estimate_inside, 100, open_estimatefile);    //fgets takes the content inside the file opened in the pointer above and saves it inside estimate_inside array
  estimate_inside[strlen(estimate_inside) - 1] = '\0';    //To remove the NULL character at the end of estimate_inside value because it might cause trouble when handling it

  all_estimates[i] = atoi(estimate_inside);    //atoi changes the data type from string to int then saves it in all_estimates array
  i++;    //it increases the value of i by 1

}

/** \fn char* check_all_estimates(char *tag_path, int sub_directory_level)
    \This Function first targets the highest sub directory level and and adds all the values found inside and stores the result in one level above. This repeats untill the sub_directory level reaches the highest level

    \This function starts searching for all the .pm_estimate files and targets one sub_directory level one at a time, then it adds all the estimates of that level and stores it one level above
*/

char* check_all_estimates(char *tag_path, int sub_directory_level)
{
  sub_directory_level++;    //increases the sub_directory_level by 1
  char file_path[1000];    //An array of type char to store the path of the .pm_estimate files as it go through all the files
  struct dirent *dp;    //defining a struct to return information about directory entries.
  DIR *dir = opendir(tag_path);   //defining a pointer of DIR data type and assigning it to open the tag_path which is the parameter of this function

  while ((dp = readdir(dir)) != NULL)    //A while loop which will run until the directory is NULL
  {
    if (dp->d_type == DT_DIR && strcmp(dp->d_name, ".") !=0 && strcmp(dp->d_name, "..") !=0)
    {
      strcpy(file_path, tag_path);    //Copy the tag_path value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value

      check_all_estimates(file_path, sub_directory_level);    //call the function recursively
    }

    else if(strcmp(dp->d_name, ".pm_estimate") == 0)
    {
      static char test_path[2024];    //defining a static array of type char
      int initial_sum[2024];    //defining a array of type int
      static int jj = 0;    //defining a variable of type int
      static int s = 0;    //defining a variable of type int
      char sum[100];  //defining an array of type char
      strcpy(file_path, tag_path);    //Copy the tag_path value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value
      if(sub_directory_level == highest_sub_directory_level-1)    //An if statement to find the sub_level which is above the highest sub_level
      {
        strcpy(ignore, dirname(file_path));    //copy the dirname of the file path in ignore array

        check_estimate_of_a_sublevel(file_path, -2);    //call the function to check all the estimtes inside the file_path
        if(test_path != file_path)    //An if statement to ensure that a file_path only gets executed once.
        {
          strcpy(test_path, file_path);    //copy the file_path in test_path array
          initial_sum[s] = 0;    //overwrite the value of initial sum by 0, so that the sum value is correct
          for(jj = 0;jj<i;jj++)    //a for loop to add all the estimates until its less than i
          {
            initial_sum[s] = initial_sum[s] + all_estimates[jj];    //add the estimates
          }
          sprintf(sum, "%d", initial_sum[s]);    //sprintf used to change the data type from int to string

          char create_estimate_file[2048];    //an array of type char to store the path of pm_estimate file to store the new sum in it
          snprintf(create_estimate_file, sizeof(create_estimate_file), "%s/.pm_estimate", ignore);    //snprintf to store the path of .pm_estimate file in create_estimate_file array

          if(initial_sum[s] != 0)    //an if statement to store the value in .pm_estimate file only if it is not 0.
          {
            FILE *open_file = fopen(("%s", create_estimate_file), "w");    //create the estimate file
            fprintf(open_file, "%s\n", sum);    //store the sum value inside the estimate file
            fclose(open_file);    //close the open_file pointer
          }
          i = 0;    //overwrite the value of i by 0
          s++;    //increases the value of s by one
         }
        }
      }
    }
  sub_directory_level--;    //decrease the vlue of sub_directory_level by 1
  closedir(dir);  //close the dir
}

/** \fn char* check_all_estimates(char *tag_path, int sub_directory_level)
    \This Function checks only one sublevel finds the .pm_estimate files and notes their estimate inside

    \This function starts searching for all the .pm_estimate files and calls other function to note their estimate inside.
*/

char *check_estimate_of_a_sublevel(char *basePath, int sub_level)
{
  sub_level++;    //increases the sub_level by 1
  char file_path[1000];    //An array of type char to store the path of the .pm_estimate files as it go through all the files
  struct dirent *dp;    //defining a struct to return information about directory entries.
  DIR *dir = opendir(basePath);   //defining a pointer of DIR data type and assigning it to open the basePath which is the parameter of this function

  while ((dp = readdir(dir)) != NULL)    //A while loop which will run until the directory is NULL
  {
    if (dp->d_type == DT_DIR && strcmp(dp->d_name, ".") !=0 && strcmp(dp->d_name, "..") !=0)//An if statement that executes if the current file is a directory or it does not begin with . or ..
    {
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value

      check_estimate_of_a_sublevel(file_path, sub_level);  //Call the function recursively
    }

    else if((strcmp(dp->d_name, ".pm_estimate") == 0) && (strcmp(basePath, ignore)) != 0)
    {
      char temp[2024];
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value
      snprintf(temp, sizeof(temp), "%s", file_path);    //snprintf to store the file_path in temp array
      if(sub_level == 0)    //An if statement to execute this only if the sub_level is 0
      {
        locate_tag(temp);    //call the function locate_tag to save the estimate inside the estimate file in an array to add them
      }
    }
  }
  closedir(dir);  //close the dir
}
