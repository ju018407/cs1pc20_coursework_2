#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>

/** \fn int create_project(char *project_name)
    \This function creates a git repository with the name entered

     This function takes a string as an input, which is the git repository name. It first checks
     whether that git repository exists or not. If not then it creates a git repository of that
     name and creates different subfolders and files inside and commits it.
*/

int create_project(char *project_name)
{
  FILE*open_project;    //A pointer used to give access to the file descriptor of the pipe

  char project_check[200];    //An array of type char to store the path to .git file inside git project
  snprintf(project_check, 200, "%s/.git", project_name);  //snprintf concatenate the strings to create the path. It has the project name, then slash, then the ".git" string.

  if(open_project = fopen(("%s", project_check), "r"))    //An if statement to check whether the .git file inside exists or not, which ultimately checks whether that git project exists or not
  {
    fclose(open_project);    //It closes the open_project pointer
    printf("A git repository of the name you enetered already exists. Aborting\n");  //Prints the string that tells the user that the git project exists
  }

  else    //Execute this else statement if the Git project does not exist
  {
    char create_gitfile[200];    //An array of type char to store the command for creating the git file with the project name
    char create_txtfile[200];    //An array of type char to store the command for creating the text file of the same project name.
    char add_txt_file[200];      //An array of type char to store the command git add the text file.
    char commit[200];    //An array of type char to store the git commit command.
    snprintf(create_gitfile, 200, "git init %s", project_name);    //snprintf concatenate the git init command and the project name and stores it in create_gitfile array
    system(create_gitfile);    //This executes the command stored in create_gitfile using the function stored in system library.

    char subfolder_name[][10] = {"bin", "doc", "lib", "src", "config", "test" };  //A 2D array of type char to store all the subfolder names that needs to be created inside git repository.

    for(int i=0;i<6;i++)    //A for loop to create the subfolders inside the project one by one as it iterates
    {
      char check_subfolders[200];  //An array of type char to store the path of the subfolders to check if they exist
      snprintf(check_subfolders, sizeof(check_subfolders), "%s/%s", project_name, subfolder_name[i]);    //snprintf concatenate the project name, slash, then the subfolder name

      if(open_project = fopen(("%s", check_subfolders), "r"))  //An if statement to check if that subfolder exists or not
      {
        i++;    //If the subfolder exists then check the next subfolder
      }

      else
      {
        char create_subfolders[200];    //An array of type char to store the path of the subfolders to create them
        snprintf(create_subfolders, 200, "%s/%s", project_name, subfolder_name[i]);  //snprintf stores the path the subfolder needs to created
        mkdir(("%s", create_subfolders), 0777);    //creating the subfolders
      }
    }
    snprintf(create_txtfile, sizeof(create_txtfile), " touch %s/%s.txt", project_name, project_name);  //snprintf stores the command to create the textfile to correct path
    system(create_txtfile);  //it executes the command which creates the text file
    chdir(project_name);  //changes the directory inside the project
    snprintf(add_txt_file, sizeof(add_txt_file), "git add %s.txt", project_name);  //snprintf stores the command to git add the newly created text file
    system(add_txt_file);  //it executes the command which git adds the text file
    snprintf(commit, sizeof(commit), "git commit -m \"initialized the %s git repository\"", project_name);  //snprintf stores the command to commit that a new repository is created
    system(commit);  //it executes the command whcih commits that a new repository has been created.
  }
  return 0; //it returns 0 which ends the function
}
