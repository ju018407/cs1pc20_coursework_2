#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>

char *getFileFormat(char *basePath, int sub_directory_level);
char file_format[1000];    //Defining a global array to store the file format or the tree structure of the project and its subfolders required for creating flowchart.

/** \fn int output_svg(char *file_name)
    \This function creates a tree diagram of svg file format of the project the user inputs.

    \This function takes a string as an input which is the project name. It first checks whether
     that project exists or not. If yes then it calls other function which goes therough all the
     files inside that project and creates a file format according to their sub directory level.
     And finally it creates a text file inside that project and stores the file format inside it
     and runs the plantuml command on that text file which creates a diagram.
*/

int output_svg(char *file_name)
{

  char plantuml_command[5000];    //Defining an array of type char to store the plantuml command.
  char fileformat_txt[1024];  //Defining an array of type char to store the path of the text file that will be created inside the project to store the image description

  FILE*open_project;    //Defining a pointer of FILE data type

  if(open_project = fopen(("%s", file_name), "r"))    //An if statement to check if the project name the user inputs exists or not.
  {
    fclose(open_project);    //closing the open_project pointer
    char path_of_project[2028];    //An array of type char to store the path of the project.
    char current_path[1024];    //An array of type char to store the path the user is currently in
    getcwd(current_path, sizeof(current_path));  //getcwd command stores the path the user is currently in, in the currrent_path array
    snprintf(path_of_project, sizeof(path_of_project), "%s/%s", current_path, file_name);    //snprintf concatenates the current path and file name and stores in path_of_project array.
    snprintf(fileformat_txt, sizeof(fileformat_txt), "%s/%s.txt", file_name, file_name);    //snprintf stores the path of the .txt file that needs to be created and stores it in fileformat_txt array
    FILE * open_fileformat_txt = fopen(fileformat_txt, "w");    //It creates the textfile, whose path is stored in fileformat_txt array
    chdir(("%s", file_name));    //changes the directory inside the project
    getFileFormat(path_of_project, 1);    //calls the getFileFormat function to create a file_format of the project

    fprintf(open_fileformat_txt, "@startwbs\n* [[file://%s %s]]\n%s@endwbs", path_of_project, file_name, file_format);    //fprintf writes the fileformat stored in file_format array inside the txt file created.
    fclose(open_fileformat_txt);    //It closes the open_fileformat_txt pointer

    snprintf(plantuml_command, sizeof(plantuml_command), "plantuml -tsvg %s/%s", current_path, fileformat_txt);    //snprintf stores the plantuml command inside the plantuml_command array
    system(plantuml_command);    //It executes the plantuml command and creates a diagram.
  }

  else
  {
    printf("The name of the project you entered does not exist. Aborting\n");    //A print statement to let the user know that the project does not exist
  }

  return 0;    //It returns 0 and ends the function
}

/** \fn char *getFileFormat(char *basePath, int sub_directory_level)
    \This function goes through all the folders and stores them in an order according to their sub level and creates a tree like format

    \This function takes a string and an int as parameter, which are the path it needs to start searching for and the sub_directory level.
*/
char *getFileFormat(char *basePath, int sub_directory_level)
{
  sub_directory_level++;    //It increases the sub_directory_level by 1
  struct dirent *dp;    //defining a struct to return information about directory entries.
  DIR *dir = opendir(".");   //defining a pointer of DIR data type and assigning it to open the current directory

  while ((dp = readdir(dir)) != NULL)    //A while loop which will run until the directory is NULL
  {
    char current_path[1024];    //An array of type char to store the current_path
    char modified_path[2028];    //An array of type char to store the remaining string required for creating diagram format
    getcwd(current_path, sizeof(current_path));    //getcwd to get the current path
    snprintf(modified_path, sizeof(modified_path), "/%s/%s %s]]", current_path, dp->d_name, dp->d_name);    //snprintf to store the required string for svg format in modified_path array

    if (dp->d_type == DT_DIR && dp->d_name[0] != '.' && strcmp(dp->d_name, "bin") && strcmp(dp->d_name, "doc") && strcmp(dp->d_name, "lib") && strcmp(dp->d_name, "src") && strcmp(dp->d_name, "test") && strcmp(dp->d_name, "config"))    //An if statement to only include the folders in file format, which we want and ignore the rest such as folders beginning with '.' or of the names bin, doc, etc.
    {
      for(int i=1;i<=sub_directory_level;i++)    //A for loop to run through the amount of times as the sub_directory_level
      {
        strcat(file_format, "*");    //Add the amount of stars as the sub directory level
      }
      strcat(file_format, " ");    // Adds a space after adding the stars
      strcat(file_format, "[[file:/");
      strcat(file_format, modified_path);
      strcat(file_format, "\n");  //Inserts a new line after adding the folder name
      chdir(dp->d_name);    //Change the directory to the current dp->d_name to look for more folders inside it
      strcpy(file_format, getFileFormat(file_format, sub_directory_level));    //Call the function recursively and copy the results in file_format
    }
  }
  chdir("..");    //Change the directory one step backwards
  sub_directory_level--;    //Decreases the sub_directory level by one
  closedir(dir);  //close the dir
  return file_format;   //it returns the file_format and ends the function
}
