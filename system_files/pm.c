#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include "my_functions.h"
#include <string.h>
#include <ctype.h>

// pm.c file will take user inputs and will call the required function after doing various error checks.


int main(int argc, char *argv[])
{
  if(argc > 1)
  {
    if(strcmp(argv[1], "help") == 0)
    {
      printf("You can input a bunch of codes, which includes:\n");
      printf("pm create_project <name> to create a git repository\n");
      printf("pm rename <current name> <new name> to rename a git repository or feature\n");
      printf("pm move <current path> <new path> to move a git repository or feature\n");
      printf("pm delete <name> to delete a git repository or feature\n");
      printf("pm email <your email> to configure your email\n");
      printf("pm name <\"full name\"> to configure your name. Please enter your full name in quotation marks\n");
      printf("pm commit <comment> to save the current stage of the git repository you are currently in\n");
      printf("pm add_url <url> to configure the url to push it to the website\n");
      printf("pm push to save the current stage to the website\n");
      printf("pm add_feature <name> to create a branch and folder of that name\n");
      printf("pm add_tag <name> to assign a tag to the current directory\n");
      printf("pm find_tag <tag> to find the path of the folder related to the tag\n");
      printf("pm move_by_tag <tag1> <tag2> to move the folder related to tag1 inside the folder related to tag2\n");
      printf("pm rename_by_tag <tag> <name> to rename the folder related to the tag\n");
      printf("pm output_svg <name> to create a svg file of the project entered\n");
      printf("pm add_estimate <tag> <name> to assign time estimate to the tag folder\n");
      printf("pm calculate_all_estimates <project name> to calculate all estimates inside the project according to their sub levels\n");
      printf("pm output_gantt <name> to create a gantt chart of the project entered\n");
    }

    else if(strcmp(argv[1], "create_project") == 0)
    {
      if(argc > 2)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ' || argv[2][i] == '/')
          {
            error_check = 1;
            printf("Bad characters in folder name\n");
            break ;
          }
        }

        if(error_check == 0)
        {
          if(argc == 3)
          {
            create_project(argv[2]);
          }

          else
          {
            printf("Wrong number of parameters, should be pm command arg\n");
          }
        }
      }

      else
      {
        printf("Error: Please enter the name of the project with the command\n");
      }
    }

    else if(strcmp(argv[1], "rename") == 0)
    {
      if(argc > 3 && argc < 5)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ' || argv[2][i] == '/')
          {
            error_check = 1;
            printf("Bad characters in current name\n");
            break;
          }

        }

        for(int i=0;i<=strlen(argv[3]);i++)
        {
          if(argv[3][i] == ' ' || argv[3][i] == '/')
          {
            error_check = 1;
            printf("Bad characters in new name\n");
            break;
          }
        }

        if(error_check == 0)
        {

          if(argc == 4)
          {
            rename_project(argv[2], argv[3]);
          }
        }
      }

      else
      {
        printf("Wrong number of parameters, should be pm command arg1 arg2\n");
      }
    }

    else if(strcmp(argv[1], "move") == 0)
    {
      if(argc > 3 && argc < 5)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ')
          {
            error_check = 1;
            printf("Bad characters in current name\n");
            break;
          }

        }

        for(int i=0;i<=strlen(argv[3]);i++)
        {
          if(argv[3][i] == ' ')
          {
            error_check = 1;
            printf("Bad characters in new name\n");
            break;
          }
        }

        if(error_check == 0)
        {

          if(argc == 4)
          {
            move_project(argv[2], argv[3]);
          }
        }
      }

      else
      {
        printf("Wrong number of parameters, should be pm command arg1 arg2\n");
      }
    }


    else if(strcmp(argv[1], "delete") == 0)
    {
      if(argc > 2)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ' || argv[2][i] == '/')
          {
            error_check = 1;
            printf("Bad characters in folder name\n");
            break ;
          }
        }

        if(error_check == 0)
        {
          if(argc == 3)
          {
            delete_project(argv[2]);
          }

          else
          {
            printf("Wrong number of parameters, should be pm command arg\n");
          }
        }
      }

      else
      {
        printf("Error: Please enter the name of the project with the command\n");
      }
    }

    else if(strcmp(argv[1], "email") == 0)
    {
      if(argc > 2)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ' || argv[2][i] == '/')
          {
            error_check = 1;
            printf("Bad characters in email\n");
            break ;
          }
        }

        if(error_check == 0)
        {
          if(argc == 3)
          {
            char email_command[200];
            snprintf(email_command, 200, "git config --global user.email \"%s\"", argv[2]);
            system(email_command);
          }

          else
          {
            printf("Wrong number of parameters, should be pm command arg\n");
          }
        }
      }

      else
      {
        printf("Error: Please enter your email with the command\n");
      }
    }

    else if(strcmp(argv[1], "name") == 0)
    {
      if(argc > 2)
      {
        if(argc == 3)
        {
          char name_command[200];
          snprintf(name_command, 200, "git config --global user.name \"%s\"", argv[2]);
          system(name_command);
        }

        else
        {
          printf("Please put your full name inside quotation marks\n");
        }
      }

      else
      {
        printf("Error: Please enter your full name inside quotation marks with the command\n");
      }
    }

    else if(strcmp(argv[1], "commit") == 0)
    {
      if(argc > 2)
      {
        if(argc == 3)
        {
          system("git add -A");
          char commit_command[200];
          snprintf(commit_command, 200, "git commit -m \"%s\"", argv[2]);
          system(commit_command);
        }

        else
        {
          printf("Please put your commit message inside quotation marks\n");
        }
      }

      else
      {
        printf("Error: Please enter the commit message inside quotation marks with the command\n");
      }
    }


    else if(strcmp(argv[1], "add_url") == 0)
    {
      if(argc > 2)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ')
          {
            error_check = 1;
            printf("Bad characters in the url\n");
            break;
          }
        }

        if(error_check == 0)
        {
          if(argc == 3)
          {
            char url_command[200];
            system("git remote remove origin");
            snprintf(url_command, 200, "git remote add origin %s", argv[2]);
            system(url_command);
          }

          else
          {
            printf("Wrong number of parameters, should be pm command arg\n");
          }
        }
      }

      else
      {
        printf("Error: Please enter the url with the command\n");
      }
    }

    else if(strcmp(argv[1], "push") == 0)
    {
      if(argc == 2)
      {
        system("git push --set-upstream origin master");
      }

      else
      {
        printf("Wrong number of parameters, should be pm push\n");
      }
    }

    else if(strcmp(argv[1], "add_feature") == 0)
    {
      if(argc > 2)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ')
          {
            error_check = 1;
            printf("Bad characters in feature name\n");
            break;
          }
        }

        if(error_check == 0)
        {
          if(argc == 3)
          {
            add_feature(argv[2]);
          }

          else
          {
            printf("Wrong number of parameters, should be pm command arg\n");
          }
        }
      }


      else
      {
        printf("Error: please enter the name of the feature with the command\n");
      }
    }

    else if(strcmp(argv[1], "add_tag") == 0)
    {
      if(argc > 2)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ' || argv[2][i] == '/')
          {
            error_check = 1;
            printf("Bad characters in feature name\n");
            break;
          }
        }

        if(error_check == 0)
        {
          if(argc == 3)
          {
            add_tag(argv[2]);
          }

          else
          {
            printf("Wrong number of parameters, should be pm command arg\n");
          }
        }
      }

      else
      {
        printf("Please enter the tag you want to add\n");
      }
    }

    else if(strcmp(argv[1], "find_tag") == 0)
    {
      if(argc > 2)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ' || argv[2][i] == '/')
          {
            error_check = 1;
            printf("Bad characters in feature name\n");
            break;
          }
        }

        if(error_check == 0)
        {
          if(argc == 3)
          {
            find_tag(argv[2]);
          }

          else
          {
            printf("Wrong number of parameters, should be pm command arg\n");
          }
        }
      }

      else
      {
        printf("Error: please enter the tag you want to find\n");
      }
    }

    else if(strcmp(argv[1], "move_by_tag") == 0)
    {
      if(argc > 3 && argc < 5)
      {
        int error_check = 0;
        int num1 = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ')
          {
            error_check = 1;
            printf("Bad characters in first tag name\n");
            break;
          }

        }

        for(int i=0;i<=strlen(argv[3]);i++)
        {
          if(argv[3][i] == ' ')
          {
            error_check = 1;
            printf("Bad characters in second tag name\n");
            break;
          }
        }

        if(error_check == 0)
        {
          if(find_tag(argv[2]) == 1)
          {
            num1 = 1;
          }

          if(find_tag(argv[3]) == 1)
          {
            num1 = 1;
          }
        }

        if(error_check == 0 && num1 == 0)
        {

          if(argc == 4)
          {
            move_by_tag(argv[2], argv[3]);
          }
        }
      }

      else
      {
        printf("Wrong number of parameters, should be pm command arg1 arg2\n");
      }
    }

    else if(strcmp(argv[1], "rename_by_tag") == 0)
    {
      if(argc > 3 && argc < 5)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ')
          {
            error_check = 1;
            printf("Bad characters in first tag name\n");
            break;
          }

        }

        for(int i=0;i<=strlen(argv[3]);i++)
        {
          if(argv[3][i] == ' ')
          {
            error_check = 1;
            printf("Bad characters in the folder name\n");
            break;
          }
        }

        if(error_check == 0)
        {
          if(argc == 4)
          {
            rename_by_tag(argv[2], argv[3]);
          }
        }
      }

      else
      {
        printf("Wrong number of parameters, should be pm command arg1 arg2\n");
      }
    }


    else if(strcmp(argv[1], "output_svg") == 0)
    {
      if(argc > 2)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ' || argv[2][i] == '/')
          {
            error_check = 2;
            printf("Bad characters in folder name\n");
            break;
          }
        }

        if(error_check == 0)
        {
          if(argc == 3)
          {
            output_svg(argv[2]);
          }

          else
          {
            printf("Wrong number of parameters, should be pm command arg\n");
          }
        }
      }
    }

    else if(strcmp(argv[1], "add_estimate") == 0)
    {
      if(argc > 3 && argc < 5)
      {
        int error_check = 0;
        int num1 = 0;
        int num2 = 0;
        char check_number[2084];
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ' || argv[2][i] == '/')
          {
            error_check = 1;
            printf("Bad characters in Tag name\n");
            break;
          }
        }

        for(int i=0;i<=strlen(argv[3]);i++)
        {
          if(argv[3][i] == ' ' || argv[3][i] == '/')
          {
            error_check = 1;
            printf("Bad characters in time estimate\n");
            break;
          }
        }

        for(int i=0;argv[3][i]!='\0';i++)
        {
          if(!(argv[3][i] == '0' || argv[3][i] == '1' || argv[3][i] == '2' || argv[3][i] == '3' || argv[3][i] == '4' || argv[3][i] == '5' || argv[3][i] == '6' || argv[3][i] == '7' || argv[3][i] == '8' || argv[3][i] == '9'))
          {
            num2 = 1;
            error_check = 1;
            printf("Please enter integers greater than 0\n");
            break;
          }
        }

        if(num2 == 0 && error_check == 0)
        {
          if(strcmp(argv[3], "0") == 0)
          {
            printf("Please enter the value more than 0\n");
            error_check = 1;
          }
        }

        if(error_check == 0)
        {
          if(find_tag(argv[2]) == 1)
          {
            num1 = 1;
          }
        }

        if(error_check == 0 && num1 == 0)
        {
          if(argc == 4)
          {
            add_estimate(argv[2], argv[3]);
          }
        }
      }

      else
      {
        printf("Wrong number of parameters, should be pm command arg1 arg2\n");
      }
    }

    else if(strcmp(argv[1], "output_gantt") == 0)
    {
      if(argc > 2)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ' || argv[2][i] == '/')
          {
            error_check = 2;
            printf("Bad characters in folder name\n");
            break;
          }
        }

        if(error_check == 0)
        {
          if(argc == 3)
          {
            output_gantt(argv[2]);
          }

          else
          {
            printf("Wrong number of parameters, should be pm command arg\n");
          }
        }
      }
    }

    else if(strcmp(argv[1], "calculate_all_estimates") == 0)
    {
      if(argc > 2)
      {
        int error_check = 0;
        for(int i=0;i<=strlen(argv[2]);i++)
        {
          if(argv[2][i] == ' ' || argv[2][i] == '/')
          {
            error_check = 1;
            printf("Bad characters in folder name\n");
            break ;
          }
        }

        if(error_check == 0)
        {
          if(argc == 3)
          {
            FILE*open_project;
            if(open_project = fopen(("%s", argv[2]), "r"))
            {
              calculate_all_estimates(argv[2]);
              printf("Operation Successful\n");
              fclose(open_project);
            }
            else
            {
              printf("The name of the project you entered does not exist. Aborting\n");
            }
          }

          else
          {
            printf("Wrong number of parameters, should be pm command arg\n");
          }
        }
      }

      else
      {
        printf("Error: Please enter the name of the project with the command\n");
      }
    }

    else
    {
      printf("Error: please enter supported arguments, for help type pm help\n");
    }
  }

  else
  {
    printf("please enter an argument such as create_project or add_feature\n");
  }

}
