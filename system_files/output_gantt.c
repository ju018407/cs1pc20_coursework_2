#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <libgen.h>

void searchThroughTheFiles(char *basePath);
char gantt_format[1000];
char project_path[1000];

/** \fn int output_gantt(char *file_name)
    \This function creates a gantt chart of the project that the user inputs, based on the
     estimates the user added in the directories and subdirectories

    \This function takes a string as an input which is the project name. It first checks whether
     that project exists or not. If yes then it calls other function which goes therough all the
     files inside that project and creates a file format necessary for a gantt chart.
     And finally it creates a text file inside that project and stores the file format inside it
     and runs the plantuml command on that text file which creates the gantt chart.
*/
int output_gantt(char *file_name)
{
  char plantuml_command[5000];    //Defining an array of type char to store the plantuml command.
  char fileformat_txt[1024];  //Defining an array of type char to store the path of the text file that will be created inside the project to store the image description

  FILE*open_project;    //Defining a pointer of FILE data type

  if(open_project = fopen(("%s", file_name), "r"))    //An if statement to check if the project name the user inputs exists or not.
  {
    fclose(open_project);    //Closing the open_project pointer
    char path_of_project[2028];    //An array of type char to store the path of the project.
    char current_path[1024];    //An array of type char to store the path the user is currently in
    getcwd(current_path, sizeof(current_path));  //getcwd command stores the path the user is currently in, in the currrent_path array
    snprintf(path_of_project, sizeof(path_of_project), "%s/%s", current_path, file_name);    //snprintf concatenates the current path and file name and stores in path_of_project array.
    snprintf(fileformat_txt, sizeof(fileformat_txt), "%s/%s.txt", file_name, file_name);    //snprintf stores the path of the .txt file that needs to be created and stores it in fileformat_txt array
    strcpy(project_path, path_of_project);    //It copies the content of path_of_project in project_path array.
    FILE * open_fileformat_txt = fopen(fileformat_txt, "w");      //It creates the textfile, whose path is stored in fileformat_txt array

    char path_of_estimate_file[2500];    //An array of type char to store the path of estimate file
    snprintf(path_of_estimate_file, sizeof(path_of_estimate_file), "%s/.pm_estimate", path_of_project);    //Snprintf to store the path of estimate file

    char estimate_in_file[100];    //An array of type char to store the estimate inside the projects estimate file
    FILE * open_estimatefile = fopen(("%s", path_of_estimate_file), "r");    //opening the file to read the content inside it
    fgets(estimate_in_file, 100, open_estimatefile);    //fgets takes the content inside the file opened in the pointer above and saves it inside estimate_in_file array
    estimate_in_file[strlen(estimate_in_file) - 1] = '\0';    //To remove the NULL character at the end of estimate_in_file value because it might cause trouble when handling it
    fclose(open_estimatefile);    //closing the open_estimatefile pointer

    chdir(("%s", file_name));    //changes the directory inside the project
    searchThroughTheFiles(path_of_project);    //calls the searchThroughTheFiles function to create a file_format of the project
    fprintf(open_fileformat_txt, "@startgantt\n-- [[file://%s %s]] lasts over %s days --\n%s@endgantt", path_of_project, file_name, estimate_in_file, gantt_format);    //fprintf writes the fileformat stored in file_format array inside the txt file created.
    fclose(open_fileformat_txt);    //It closes the open_fileformat_txt pointer

    snprintf(plantuml_command, sizeof(plantuml_command), "plantuml -tsvg  %s/%s", current_path, fileformat_txt);    //snprintf stores the plantuml command inside the plantuml_command array
    system(plantuml_command);    //It executes the plantuml command and creates a diagram.
  }

  else
  {
    printf("The name of the project you entered does not exist. Aborting\n");    //A print statement to let the user know that the project does not exist
  }

  return 0;    //It returns 0 and ends the function
}

/** \fn void searchThroughTheFiles(char *basePath)
    \This function goes through all the folders and look for all the .pm_estimate files. It creates a format necessary for gantt chart with the subfolder names associated with their .pm estimate files and the estimate values. 

    \This function takes a string as parameter, which is the path it needs to start searching for
*/

void searchThroughTheFiles(char *basePath)
{
  char file_path[1000];    //An array of type char to store the path of all the files while searching for .pm_estimate files
  struct dirent *dp;        //defining a struct to return information about directory entries.
  DIR *dir = opendir(basePath);   //defining a pointer of DIR data type and assigning it to open the basePath which is the parameter of this function

  while ((dp = readdir(dir)) != NULL)    //A while loop which will run until the directory is NULL
  {
    if (dp->d_type == DT_DIR && strcmp(dp->d_name, ".") !=0 && strcmp(dp->d_name, "..") !=0)//An if statement that executes if the current file is a directory or it does not begin with . or ..
    {
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value

      searchThroughTheFiles(file_path);  //Call the function recursively
    }

    else if((strcmp(dp->d_name, ".pm_estimate") == 0) && strcmp(basePath, project_path))
    {
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);  //Call the function recursively

      FILE*open_estimatefile;    //defining a pointer of FILE data type to open the tag files
      char estimate_inside[100];    //An array of type char to store the tag found inside the .pm_tag file
      char hyperlink[4000];
      open_estimatefile = fopen(("%s", file_path), "r");    //opening the file to read the content inside it
      fgets(estimate_inside, 100, open_estimatefile);    //fgets takes the content inside the file opened in the pointer above and saves it inside tag_in_file
      estimate_inside[strlen(estimate_inside) - 1] = '\0';    //To remove the NULL character at the end of tag_in_file value because it might cause trouble when handling it
      fclose(open_estimatefile);

      char file_name[1028];    //An array of type char to store the file name
      snprintf(file_name, sizeof(file_name), "%s", basename(dirname(file_path)));    //snprintf to store the filename in array

      strcat(gantt_format, "[");    //Add a '[' in the gantt_format array
      strcat(gantt_format, file_name);    //Add the folder name which is associated with the estimate file inside gantt_format
      char estimate_in_text[1000];    //An array of type char to store the remaining text necessary to create gantt format
      snprintf(estimate_in_text, sizeof(estimate_in_text), "] lasts %s days", estimate_inside);    //snprintf to store the text and the estimate required for creating a gantt chart
      snprintf(hyperlink, sizeof(hyperlink), "[%s] links to [[%s]]", file_name, file_path);
      strcat(gantt_format, estimate_in_text);    //Adding the content of estimate_in_text array inside gantt_format
      strcat(gantt_format, "\n");    //Add a new line at the end of content of gantt_format array
      strcat(gantt_format, hyperlink);
      strcat(gantt_format, "\n");    //Add a new line at the end of content of gantt_format array

    }
  }
  closedir(dir);  //close the dir
}
