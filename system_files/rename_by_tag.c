#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <stdbool.h>
#include <libgen.h>
#include "my_functions.h"

void checkThroughFiles(char *basePath, char *tag, char *new_name);

/** \fn rename_by_tag(char *tag, char *new_name)
    \This function renames the project that is associated with the tag.

    \This function takes two string as an input which are the tag and the new name of the project
     It first checks whether the tag exists or not. If yes then it finds the path of the folder
     associated with the tag. And at last it renames the folder with the new name.
*/

int rename_by_tag(char *tag, char *new_name)
{
  char current_path[1000];    //An array of type char to store the current path
  char start_path[1000];  //An array of type char which stores the path to start searching for when searching the tag.

  getcwd(current_path, sizeof(current_path));    //getcwd to store the current path in array

  char * token = strtok(current_path, "/");    //strtok used to get the first part of the path used to search tag
  snprintf(start_path, sizeof(start_path), "/%s", token);    //snprintf to add '/' at the start to start searching for tag

  if(find_tag(tag) == 0)    //calling the find_tag function to check if the tag exists or not.
  {
    checkThroughFiles(start_path, tag, new_name);    //If the tag exists then Calling the function checkThroughFiles, to start searching for tag and find its path
  }

  return 0;   //It returns 0 and ends the function
}

/** \fn void checkThroughFiles(char *basePath, char *tag, char *new_name)

    \This function searches through all files and folders for all .pm_tag files, and rename that tag folder with the new name
*/

void checkThroughFiles(char *basePath, char *tag, char *new_name)
{
  char file_path[1000];  //An array of type char to store the path of the .pm_estimate files as it go through all the files
  struct dirent *dp;      //defining a struct to return information about directory entries.
  char new_name_path[5000];    //An array of type char to store the path of the tag with the new name. 
  char dirname_of_tag[5000];    //An array of type char to store the path of the dirname of the tag folder path
  int ret;    //Defining an int which will have the rename function.
  DIR *dir = opendir(basePath);   //defining a pointer of DIR data type and assigning it to open the basePath which is the parameter of this function

  if(!dir)  //If it is Unable to open directory stream
  {
    return;  //This ends the function.
  }

  while ((dp = readdir(dir)) != NULL)    //A while loop which will run until the directory is NULL
  {
    if (dp->d_type == DT_DIR && strcmp(dp->d_name, ".") !=0 && strcmp(dp->d_name, "..") !=0)    //An if statement that executes if the current file is a directory or it does not begin with . or ..
    {
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");    //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value

      checkThroughFiles(file_path, tag, new_name);  //Call the function recursively
    }

    else if(strcmp(dp->d_name, ".pm_tag") == 0)    //Execute this if the dp-d_name is a .pm_tag file
    {
      strcpy(file_path, basePath);    //Copy the basePath value to file_path
      strcat(file_path, "/");   //Add a '/' at the end of the file_path value
      strcat(file_path, dp->d_name);    //Add the dp->d_name value at the end of the file_path value

      FILE*open_tagfile;    //defining a pointer of FILE data type to open the tag files
      char tag_found_inside_file[100];    //An array of type char to store the tag found inside the .pm_tag file

      open_tagfile = fopen(("%s", file_path), "r");    //opening the file to read the content inside it
      fgets(tag_found_inside_file, 100, open_tagfile);    //fgets takes the content inside the file opened in the pointer above and saves it inside tag_in_file
      tag_found_inside_file[strlen(tag_found_inside_file) - 1] = '\0';    //To remove the NULL character at the end of tag_in_file value because it might cause trouble when handling it
      fclose(open_tagfile);

      if(strcmp(tag_found_inside_file, tag) == 0)    //An if statement which gets execute if the tag found in file is equal to the tag the user inputs
      {
        int file_name_check = 0;  //Declaring an int to check if a project with the same name already exists as the newname of the tag folder the user inputs
        snprintf(dirname_of_tag, sizeof(dirname_of_tag), "%s", basePath);  //snprintf to store the basePath in dirname_of_tag array
        snprintf(new_name_path, sizeof(new_name_path), "%s/%s", dirname(dirname_of_tag), new_name); //snprintf to store the path of the tag with the new name.

        FILE*open_newname_file;  //defining a pointer of FILE data type to open the tag path with the new name
        if(open_newname_file = fopen(("s", new_name_path), "r"))  //An if statement to check if a project with the same name already exists as the newname of the tag folder the user inputs
        {
          fclose(open_newname_file);
          file_name_check = 1;    //Overwriting the value of file_name_check to 1
          printf("A file of that name already exists. Aborting\n");    //A print statement to let the the user know a file with the same name already exists
        }

        if(file_name_check == 0)  //An if statement to execute if the value of file_name_check is 0, then execute it, which means that the file with the same name does not exist
        {
          ret = rename(basePath, new_name_path);    //this is to rename the project the user wants to
        }
      }
    }
  }
  closedir(dir);  //close the dir
}
